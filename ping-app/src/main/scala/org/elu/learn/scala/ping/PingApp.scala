package org.elu.learn.scala.ping

import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.IpLiteralSyntax
import org.http4s.server.Router
import org.http4s.implicits._
import org.http4s.ember.server.EmberServerBuilder

object PingApp extends IOApp {
  private val httpApp = Router(
    "/" -> new PingApi().routes
  ).orNotFound

  override def run(args: List[String]): IO[ExitCode] =
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8000")
      .withHttpApp(httpApp)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)

}
