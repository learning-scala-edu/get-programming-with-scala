package org.elu.learn.scala.timePrinter

import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

class TimePrinter(formatter: DateTimeFormatter) {
  def now(country: String): String = {
    val timezone = countryToTimezone(country)
    val dateTime = currentDateTime(timezone)
    dateTimeToString(dateTime)
  }

  private def countryToTimezone(country: String): String =
    country.toLowerCase match {
      case "italy" => "Europe/Rome"
      case "uk" => "Europe/London"
      case "germany" => "Europe/Berlin"
      case "switzerland" => "Europe/Zurich"
      case "finland" => "Europe/Helsinki"
      case "japan" => "Asia/Tokyo"
      case _ =>
        val msg = s"Unknown timezone for country $country"
        throw new IllegalArgumentException(msg)
    }

  private def currentDateTime(timezone: String): ZonedDateTime =
    ZonedDateTime.now(ZoneId.of(timezone))

  private def dateTimeToString(dateTime: ZonedDateTime): String =
    formatter.format(dateTime)
}
