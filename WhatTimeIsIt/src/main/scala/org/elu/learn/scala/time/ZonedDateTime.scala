package org.elu.learn.scala.time

import java.time.format.DateTimeFormatter
import scala.io.StdIn

object ZonedDateTime extends App {
  val timeZone = StdIn.readLine("Give me a timezone: ")
  var timePrinter = new TimePrinter(DateTimeFormatter.RFC_1123_DATE_TIME)
  println(timePrinter.now(timeZone))
}
