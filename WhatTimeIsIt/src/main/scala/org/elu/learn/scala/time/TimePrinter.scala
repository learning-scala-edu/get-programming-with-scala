package org.elu.learn.scala.time

import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

class TimePrinter(formatter: DateTimeFormatter) {

  def now(timeZone: String): String = {
    val dateTime = currentDateTime(timeZone)
    dateTimeToString(dateTime)
  }

  private def currentDateTime(timeZone: String): ZonedDateTime = {
    val zoneId = ZoneId.of(timeZone)
    ZonedDateTime.now(zoneId)
  }

  private def dateTimeToString(dateTime: ZonedDateTime): String = {
    formatter.format(dateTime)
  }

}
